#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o xtrace
shopt -s expand_aliases

PIP_PKGS="
hatch
commitizen
httpx
python-lsp-ruff
python-lsp-server
ruff
"

if command -v yay
then
    alias install='yay -S --needed --confirm'
    alias uninstall='yay -Rns --confirm'
elif command -v pacman
then
    alias install='sudo pacman -S --needed --confirm'
    alias uninstall='sudo pacman -Rns --confirm'
elif command -v apt
then
    alias install='sudo apt install --no-install-recommends --no-install-suggests'
    alias uninstall='sudo apt remove --purge --auto-remove'
fi


snap_clean() {
    snap remove $(snap list|grep -vw snap)
    snap remove snap
    uninstall snapd
}

install_dotfiles() {
    # clone somewhat ignores --work-tree and --git-dir.
    # It will always create the target dir (either ${REPO_NAME} or last arg),
    # whatever the flags are.
    # This dir must not already exist with content.
    # If --git-dir and --no-checkout is specified, it will ignore the value of
    # git-dir and place repo metadata directly inside the target dir
    git --work-tree=$(mktemp -d --dry-run) clone --recursive --no-checkout git@gitlab.com:ribetm/dotfiles $HOME/.dotfiles
    git --git-dir=$HOME/.dotfiles --work-tree=$HOME submodule update --init --remote --recursive
}

install_pyenv() {
    install $(get_pkgs python_build)
    command -v pyenv || {
        export PATH="$HOME/.pyenv/bin:$PATH"
        eval "$(pyenv init -)"
        eval "$(pyenv virtualenv-init -)"
    }
    pyenv install --skip-existing 3
    pyenv exec pip install -U pip
    pyenv exec pip install -U setuptools wheel
    pyenv exec pip install -U $PIP_PKGS
}

setup_gpg_agent() {
    export GPG_TTY=$(tty)
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
    gpgconf --launch gpg-agent
}

ask() {
    while true
    do
        read -e -p "$1 ? [y/n]: " -i "$2" answer
        case ${answer} in
            y|Y) return 0;;
            n|N) return 1;;
        esac
    done
}

recommend() { ask "$1" "y"; }
suggest() { ask "$1" "n"; }

# main

get_pkgs() {
    grep $(lsb_release -is) $1.txt |  cut -d $'\t' -f1
}

deb_sources() {
    sudo wget -N https://baltocdn.com/helm/signing.asc -O /etc/apt/keyrings/helm.asc
    sudo wget -N https://packages.mozilla.org/apt/repo-signing-key.gpg -O /etc/apt/keyrings/mozilla.asc
    sudo wget -N https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key -O /etc/apt/keyrings/kubernetes.asc
    sudo wget -N https://apt.releases.hashicorp.com/gpg -O /etc/apt/keyrings/hashicorp.asc
    sudo chmod 644 /etc/apt/keyrings/*
    sudo cp apt_sources.txt /etc/apt/sources.list.d/sources.list
    sudo apt update
    echo '
Package: *
Pin: origin packages.mozilla.org
Pin-Priority: 1000
' | sudo tee /etc/apt/preferences.d/mozilla
}

main() {
    recommend "WARNING: is the current shell $SHELL the one you will use ?" || exit

    command -v apt && deb_sources

    recommend "Install core packages" && install $(get_pkgs core)
    suggest "Install work env" && {
        setup_gpg_agent
        recommend "Install dotfiles" && install_dotfiles
        install $(get_pkgs work)
        suggest "Install Python env" && install_pyenv
        suggest "Install Sway env" && install $(get_pkgs sway)
    }

    recommend "Remove garbage" && {
        uninstall $(cat unwanted.txt) || true
        command -v snap && snap_clean
    }


    getent group docker && sudo usermod -a -G docker $(whoami)
    getent group wireshark && sudo usermod -a -G wireshark $(whoami)

    command -v docker && sudo systemctl enable --now docker
}

main
